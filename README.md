Instructions for compiling my AerStack class, and running the tests for said stack. 

1. Ensure that your maven version is up to date. If it's not available, then run sudo apt-get install maven

2. Run "mvn package" inside the folder with pom.xml. This will set up all the dependencies for the class and for the junit testing. Also, it will run all the test cases I wrote for the class

3. The source code for AerStack.java that implements SweetStack.java can be found in "~/src/main/java/com/aerserv/stack". The tests can be found at "~/src/test/java/com/aerserv/stack/TestAerStack.java".
The jar file is at "~/target/aerstack-1.0-SNAPSHOT.jar". Class Files are found in "~/target/classes/com/aerserv/stack/". 
