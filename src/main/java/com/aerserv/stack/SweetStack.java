package com.aerserv.stack;
/*
* The interface of a stack data structure that will have 4 public operations that complete in O(1) time complexity.
* These operations are push(int val), pop(void), size(void), max(void).
* The purpose of a stack data structure is based on the principle of data accessibility called First in Last Out
* such that any integer placed in the stack is kept in order of being placed into the stack, and removed by the most
* recently placed item first to eventually the last placed item last.
*
* @author Jason Lee
* @date 2/25/2017
*
*/

public interface SweetStack {

    /*
    * Takes an input integer value, val, and places it within the stack.
    *
    * @param val : An integer value that can be arbitrary
    * @return void
    */
    public void push(int val);

    /*
    * Removes the most recently input integer value through the push operation from the top, and returns
    * it.
    *
    * @return an integer
    */
    public int pop();

    /*
    * Returns the number of entries inside the stack.
    *
    * @return an integer
    */
    public int size();

    /*
    * Returns the currently largest integer value within the stack.
    *
    * @return an integer
    */
    public int max();
}

