package com.aerserv.stack;

import java.util.NoSuchElementException;

/*
* The implementation of SweetStack that will have 4 public operations that complete in O(1) time complexity.
* These operations are push(int val), pop(void), size(void), max(void). This implementation is not thread-safe.
*
* @author Jason Lee
* @date 2/25/2017
*
*/
public class AerStack implements SweetStack {
    private int numberOfEntries = 0;
    private LinkedNode entries, largestEntries = null;

    /*
    * Using the idea of a LinkedList, nodes are pushed onto the stack in the order of their arrival in constant time
    * since it requires only updating entries to the newly placed LinkedNode in the stack.
    * All previous forms of entries will be remembered by the next field in the LinkedNode.
    * In order to know the largest item in the stack, we have to create another LinkedNode maintaining
    * equivalently large or larger items that have entered into entries.
    */
    public void push(int val) {
        if (entries == null) {
            entries = new LinkedNode(val, null);
            largestEntries = new LinkedNode(val, null);
        } else {
            entries = new LinkedNode(val, entries);
            if (largestEntries.getValue() <= val) {
                largestEntries = new LinkedNode(val, largestEntries);
            }
        }
        numberOfEntries++;
    }

    /*
    * To remove items from the stack, the only requirement is to set our entries to the next field of our top-most
    * LinkedNode object. The user could call a pop() operation without any objects inside the stack, which should
    * throw a NoSuchElementException to the user.
    */
    public int pop() {
        if (numberOfEntries == 0) {
            throw new NoSuchElementException("The stack is exhausted.");
        }
        int topEntry = entries.getValue();
        entries = entries.getNext();
        if (largestEntries.getValue() == topEntry) {
            largestEntries = largestEntries.getNext();
        }
        numberOfEntries--;
        return topEntry;
    }

    /**
     * In order to have O(1) size operations with a linked list data structure, numberOfEntries is maintained whenever
     * a LinkedNode is pushed and popped off the stack.
     */
    public int size() {
        return numberOfEntries;
    }

    /**
     * By maintaining another LinkedNode stack, we can remember previous largest integers if we pop off any largest
     * integers. Because any items placed in this stack will be greater than or equal to the previous
     * largest integer, there is no need to find the max element inside entries in O(n) time,
     * and we guarantee that any time, we pop off the largest item in entries,
     * we can have no larger item in entries than in largestEntries.
     *
     */
    public int max() {
        if (numberOfEntries == 0) {
            throw new NoSuchElementException("No elements in the stack.");
        }
        return largestEntries.getValue();
    }

    private class LinkedNode {
        private int val;
        private LinkedNode next;

        public LinkedNode() {
            val = 0;
            next = null;
        }

        public LinkedNode(int val, LinkedNode next) {
            this.val = val;
            this.next = next;
        }

        public int getValue() {
            return val;
        }

        public void setValue(int val) {
            this.val = val;
        }

        public LinkedNode getNext() {
            return next;
        }

        public void setNext(LinkedNode next) {
            this.next = next;
        }
    }


}
