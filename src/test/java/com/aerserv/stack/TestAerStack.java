package com.aerserv.stack;

import org.junit.Test;

import java.util.NoSuchElementException;

import static org.junit.Assert.*;

/**
 * Created by Jason on 2/26/2017.
 */
public class TestAerStack {

    @Test
    public void testPush() {
        try {
            AerStack stack = new AerStack();
            int[] testValues = {1, -1, 0, 4, 2000000000, -2000000000, 1, -1};
            for (int value : testValues) {
                stack.push(value);
            }
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testPop() {
        AerStack stack = new AerStack();
        int[] testValues = {1, -1, 0, 4, 2000000000, -2000000000, 1, -1};
        for (int value : testValues) {
            stack.push(value);
        }
        for (int index = testValues.length -1 ; index >= 0; index--) {
            assertEquals(testValues[index], stack.pop());
        }
        try {
            stack.pop();
            fail();
        } catch (NoSuchElementException e) {

        }
    }

    @Test
    public void testNormalIncrementation() {
        AerStack stack = new AerStack();

        for (int i = 0; i < 10; i++) {
            stack.push(i);
        }
        for (int i = 9; i >= 0; i--) {
            int returnValue = stack.pop();
            assertEquals(i, returnValue);
        }
    }

    @Test
    public void testPushPop() {
        AerStack stack = new AerStack();

        stack.push(0);
        assertEquals(0, stack.pop());
        for (int i = 500; i < 600; i = i + 10) {
            stack.push(i);
            assertEquals(i, stack.pop());
        }
        stack.push(-1000);
        stack.push(500);
        assertEquals(500, stack.pop());
        assertEquals(-1000, stack.pop());
    }

    @Test
    public void testSize() {
        AerStack stack = new AerStack();

        for (int i = 0; i < 100; i++) {
            stack.push(i);
            assertEquals(i + 1, stack.size());
        }

        assertEquals(100, stack.size());
        assertEquals(100, stack.size());

        for (int i = 99; i >= 0; i--) {
            stack.pop();
            assertEquals(i, stack.size());
        }
    }
    @Test
    public void testMultipleCopiesMax() {
        AerStack stack = new AerStack();
        for (int i = 0; i < 10; i++ ){
            stack.push(1);
            assertEquals(1, stack.max());
        }
        for (int i = 1; i < 10; i++) {
            stack.pop();
            assertEquals(1, stack.max());
        }

    }

    @Test
    public void testNoMax() {
        AerStack stack = new AerStack();
        try {
            stack.max();
            fail();
        } catch (NoSuchElementException e) {

        }
    }
    @Test
    public void testMovingMax() {
        AerStack stack = new AerStack();
        int[] testValues = {-400, -300, 5,4,3,2,1,10,20,500};

        for (int i = 0; i < 3; i++) {
            stack.push(testValues[i]);
            assertEquals(testValues[i], stack.max());
        }
        stack.push(testValues[3]);
        assertEquals(testValues[2], stack.max());

        for (int i = 4; i < 7; i++) {
            stack.push(testValues[i]);
            assertEquals(testValues[2], stack.max());
        }

        for (int i = 7; i < 10; i++) {
            stack.push(testValues[i]);
            assertEquals(testValues[i], stack.max());
        }

        for (int i = 9; i >= 8; i--) {
            stack.pop();
            assertEquals(testValues[i - 1], stack.max());

        }
        stack.pop();

        for (int i = 0; i < 5; i++) {
            assertEquals(testValues[2], stack.max());
            stack.pop();
        }

        assertEquals(testValues[1], stack.max());
        stack.pop();
        assertEquals(testValues[0], stack.max());

    }

    @Test
    public void testNoStaticElements() {
        AerStack s1 = new AerStack();
        AerStack s2 = new AerStack();
        for (int i = 0; i < 10; i++) {
            s1.push(i);
        }
        assertEquals(9, s1.max());

        try {
            s2.max();
            fail();
        } catch (NoSuchElementException e) {}

        try {
            s2.pop();
            fail();
        } catch (NoSuchElementException e) {}

        for (int j = 0; j < 10; j++) {
            s2.push(j * 10);
        }
        s2.push(-400);
        s2.pop();
        assertTrue(s1.size() == 10);

        for (int bothIndices = 9; bothIndices >= 0; bothIndices--) {
            assertTrue(s1.pop() * 10 == s2.pop());
        }
    }

    @Test
    public void testLargeNumberOfInputs() {
        AerStack stack = new AerStack();
        int largeNumber = 1000000;
        for (int i = 0 ; i < largeNumber; i++) {
            stack.push(i);
            assertTrue(stack.size() == i + 1);
        }

        assertEquals(largeNumber - 1, stack.max());

        try {
            for (int i = 0; i < largeNumber; i++) {
                stack.pop();
            }
        } catch (NoSuchElementException e) {
            fail();
        }


    }
}
